import UIKit
import GoogleMaps

class ViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {

  @IBOutlet weak var googleMapView: GMSMapView!
  
  var locationManager = CLLocationManager()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.locationManager.delegate = self
    self.locationManager.startUpdatingLocation()
    createLine()
  }


  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    let location = locations.last
    let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
    self.googleMapView?.animate(to: camera)
    self.locationManager.stopUpdatingLocation()
  }
  
  func createLine(){
    let path = GMSMutablePath()
    path.add(CLLocationCoordinate2D(latitude: 13.744254761738812 , longitude: 100.55121207406536))
    path.add(CLLocationCoordinate2D(latitude: 13.844254761738812, longitude: 101.55121207406536))
    path.add(CLLocationCoordinate2D(latitude: 13.944254761738812, longitude: 102.55121207406536))
    path.add(CLLocationCoordinate2D(latitude: 13.644254761738812, longitude: 103.55121207406536))
    path.add(CLLocationCoordinate2D(latitude: 13.344254761738812, longitude: 108.55121207406536))

    let rectangle = GMSPolyline(path: path)
    rectangle.map = googleMapView
  }
}

